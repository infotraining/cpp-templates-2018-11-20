#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>
#include <list>

#include "catch.hpp"

using namespace std;

namespace Step1
{
    template <typename T>
    T my_accumulate(const T* first, const T* last)
    {
        T total = T{};

        for (auto it = first; it != last; ++it)
        {
            total += *it;
        }

        return total;
    }
}

namespace Step2
{
   template <typename T>
   struct AccumulationTraits
   {
       using AccumulatorType = T;
       static const AccumulatorType zero; // declaration
   };

   // definition
   template <typename T>
   const T AccumulationTraits<T>::zero{};

   template <>
   struct AccumulationTraits<uint8_t>
   {
       typedef uint64_t AccumulatorType;
       static const AccumulatorType zero = 0;
   };

   template <>
   struct AccumulationTraits<string>
   {
       typedef string AccumulatorType;
       static constexpr const char* zero = "string: ";
   };

   // alias for template
   template <typename T>
   using AccumulationTraits_t = typename AccumulationTraits<T>::AccumulatorType;

   template <typename Iterator,
             typename AccT = AccumulationTraits_t<typename iterator_traits<Iterator>::value_type>>
   AccT my_accumulate(Iterator first, Iterator last)
   {
       using T = typename iterator_traits<Iterator>::value_type;
       AccT total = AccumulationTraits<T>::zero;

       for (auto it = first; it != last; ++it)
       {
           total += *it;
       }

       return total;
   }
}

TEST_CASE("accumulate")
{
    const list<uint8_t> tab = { 1, 2, 255, 4, 5 };

    REQUIRE(Step2::my_accumulate(begin(tab), end(tab)) == 267);

    string words[] = { "one", "two", "three" };

    REQUIRE(Step2::my_accumulate(begin(words), end(words)) == "string: onetwothree");

    double numbers[] = {3.14, 6.1};

    REQUIRE(Step2::my_accumulate(begin(numbers), end(numbers)) == Approx(9.24));
}
