#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

string full_name(const string& first, const string& last)
{
    return first + " " + last;
}

vector<string> create_words()
{
    vector<string> vec;

    string name = "Ewa";

    vec.push_back(name);
    vec.push_back(name + " Kowalska");
    vec.push_back(full_name(name, "Nowak"));
    vec.push_back("Ola Nowak");
    vec.push_back(std::move(name));

    return vec;
}

TEST_CASE("reference bindings")
{
    string name = "Jan";

    SECTION("C++98")
    {
        string& ref_name = name;

        const string& cref_full_name = full_name(name, "Kowalski");

        //cref_full_name[0] = 'P';
    }

    SECTION("C++11")
    {
        string&& refref_full_name = full_name(name, "Kowalski");

        refref_full_name[0] = 'P';

        REQUIRE(refref_full_name == "Pan Kowalski"s);

        // string&& refref = name; // ERROR - ill formed
    }
}

TEST_CASE("vector returned from function")
{
    vector<string> words = create_words();
}

namespace kplus
{
    template <typename T>
    class K_Vector_c
    {
        T* items_;
        size_t size_;

        using iterator = T*;

    public:
        explicit K_Vector_c(size_t size)
            : items_(new T[size])
            , size_(size)
        {
            fill_n(items_, size_, T{});
            cout << "K_Vector_c(size: " << size_ << ", items: " << items_ << ")\n";
        }

        // move constructor
        K_Vector_c(K_Vector_c&& source)
            : items_(source.items_)
            , size_(source.size_)
        {
            cout << "K_Vector_c(mv size: " << size_ << ", items: " << items_ << ")\n";
            source.items_ = nullptr;
        }

        // move assignment
        K_Vector_c& operator=(K_Vector_c&& source)
        {
            if (this != &source)
            {
                delete[] items_;

                items_ = source.items_;
                size_ = source.size_;

                source.items_ = nullptr;
            }

            return *this;
        }

        ~K_Vector_c()
        {
            cout << "~K_Vector_c(size: " << size_ << ", items: " << items_ << ")\n";
            delete[] items_;
        }

        T& operator[](size_t index)
        {
            return items_[index];
        }

        iterator begin()
        {
            return items_;
        }

        iterator end()
        {
            return items_ + size_;
        }
    };
}

kplus::K_Vector_c<string> load_data()
{
    kplus::K_Vector_c<string> data(1000000);

    data[0] = "First";
    data[1] = "Second";

    return data;
}

namespace kplus
{
    struct K_Data_c
    {
        K_Vector_c<int> items;
        vector<string> vec;
        string s;
    };
}

TEST_CASE("K_Vector_c")
{
    using namespace kplus;

    K_Vector_c<int> vec(10);
    vec[0] = 1;
    vec[5] = 5;

    for (const auto& item : vec)
        cout << item << " ";
    cout << '\n';

    K_Vector_c<string> vec2 = load_data();
    //K_Vector_c<string> backup_vec2 = vec2;

    vector<K_Vector_c<string>> data;
    data.push_back(load_data());
    data.push_back(std::move(vec2));
}
