#include <algorithm>
#include <iostream>
#include <list>
#include <numeric>
#include <string>
#include <vector>
#include <type_traits>

#include "catch.hpp"

using namespace std;

template <typename Iterator>
void advance_iter_impl(Iterator& it, size_t n, std::input_iterator_tag)
{
    cout << "advance_iter_impl for input iterator" << endl;
    for (size_t i = 0; i < n; ++i)
        ++it;
}

template <typename Iterator>
void advance_iter_impl(Iterator& it, size_t n, std::random_access_iterator_tag)
{
    cout << "advance_iter_impl for random access iterator" << endl;
    it += n;
}

template <typename Iterator>
void advance_iter(Iterator& it, size_t n)
{
    advance_iter_impl(it, n, typename iterator_traits<Iterator>::iterator_category{});
}

namespace AlternativeTake
{
        template <typename Iterator>
        using IsRandomAccessIterator = integral_constant<bool,
                is_same<
                    typename iterator_traits<Iterator>::iterator_category,
                    random_access_iterator_tag
                >::value
        >;


        template <typename Iterator>
        void advance_iter_impl(Iterator& it, size_t n, std::false_type)
        {
            cout << "advance_iter_impl for input iterator" << endl;
            for (size_t i = 0; i < n; ++i)
                ++it;
        }

        template <typename Iterator>
        void advance_iter_impl(Iterator& it, size_t n, std::true_type)
        {
            cout << "advance_iter_impl for random access iterator" << endl;
            it += n;
        }

        template <typename Iterator>
        void advance_iter(Iterator& it, size_t n)
        {
            advance_iter_impl(it, n, IsRandomAccessIterator<Iterator>{});
        }
}

namespace AlternativeTakeCpp17
{
    template <typename Iterator>
    void advance_iter(Iterator& it, size_t n)
    {
        if constexpr (is_same_v<typename iterator_traits<Iterator>::iterator_category, random_access_iterator_tag>)
        {
            it += n;
        }
        else
        {
            for (size_t i = 0; i < n; ++i)
                ++it;
        }
    }
}

TEST_CASE("tag-dispatch")
{
    SECTION("for list")
    {
        list<int> lst = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        auto it = lst.begin();

        AlternativeTake::advance_iter(it, 4);

        REQUIRE(*it == 5);
    }

    SECTION("for vector")
    {
        vector<int> vec = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        auto it = vec.begin();

        AlternativeTake::advance_iter(it, 4);

        REQUIRE(*it == 5);
    }
}
