#include <algorithm>
#include <cstring>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename T>
T max(T a, T b)
{
    cout << "Generic ";
    puts(__PRETTY_FUNCTION__);
    return a < b ? b : a;
}

template <typename T>
T* max(T* a, T* b)
{
    cout << "For pointers: ";
    puts(__PRETTY_FUNCTION__);
    return *a < *b ? b : a;
}

const char* max(const char* txt1, const char* txt2)
{
    cout << "max(const char*, const char*) - specialized\n";
    return std::strcmp(txt1, txt2) < 0 ? txt2 : txt1;
}

TEST_CASE("template functions")
{
    int x = 10;
    int y = 20;

    SECTION("explicit template instantiation")
    {
        ::max<int>(x, y);
    }

    SECTION("implicit template instantiation - using type deduction")
    {
        auto res1 = ::max(3.14, 6.65); // max<double>(double, double)
        REQUIRE(res1 == Approx(6.65));

        auto res2 = ::max("text"s, "abc"s);
        REQUIRE(res2 == "text");
    }

    SECTION("ambigous call")
    {
        auto res1 = ::max(3.14, static_cast<double>(5));
        REQUIRE(res1 == Approx(5.0));

        auto res2 = ::max<double>(3.14, 5);
        REQUIRE(res2 == Approx(5.0));
    }
}

TEST_CASE("template specialization")
{
    int x = 10;
    int y = 5;

    SECTION("for pointers")
    {
        auto res = ::max(&x, &y);

        REQUIRE(*res == 10);
    }

    SECTION("char*")
    {
        cout << "test char*: ";
        char c1 = 'a';
        char c2 = 'b';

        auto res = ::max(&c1, &c2);

        REQUIRE(*res == 'b');
    }

    SECTION("for c-strings")
    {
        cout << "test c-string: ";
        auto res = ::max("aabcd", "aextd");

        cout << "max for c-string: " << res << endl;
    }
}

auto foo(int x)
{
    if (x % 2 == 0)
        return string("even");

    return "odd"s;
}

template <typename TResult, typename T1, typename T2>
TResult multiply(const T1& a, const T2& b)
{
    return a * b;
}

namespace cpp11
{
    template <typename T1, typename T2>
    auto multiply(const T1& a, const T2& b) //-> decltype(a * b)
    {
        return a * b;
    }
}

vector<double> operator*(const vector<int>& vec, double factor)
{
    vector<double> result(begin(vec), end(vec));

    for(auto& item : result)
        item *= factor;

//    for(auto it = vec.begin(); it != vec.end(); ++it)
//    {
//        auto& item = *it;
//        item *= factor;
//    }

    return result;
}

namespace traits
{
    template <typename T1, typename T2>
    typename std::common_type<T1, T2>::type multiply(const T1& a, const T2& b)
    {
        return a * b;
    }
}

namespace std
{
    template <>
    struct common_type<vector<int>, double>
    {
        using type = vector<double>;
    };
}

TEST_CASE("multiply")
{
    auto res1 = multiply<int>(2, 3); // multiply<int, int, int>(...)
    REQUIRE(res1 == 6);

    auto res2 = multiply<int, double>(3.14, 7); // multiply<int, double, int>
    REQUIRE(res2 == 21);
}

TEST_CASE("cpp11::multiply")
{
    auto res1 = cpp11::multiply(2, 4);

    REQUIRE(res1 == 8);
}

TEST_CASE("traits")
{
    auto res1 = traits::multiply(3.14, 2);
    REQUIRE(res1 == Approx(6.28));

    vector<int> vec = {1, 2, 3};
    auto res2 = traits::multiply(vec, 3.1);
}

template <typename Container>
typename Container::iterator my_begin(Container& c)
{
    return c.begin();
}

template <typename Container>
auto my_end(Container& c) -> decltype(c.end())
{
    return c.end();
}

template <typename T, size_t N>
auto my_begin(T (&arr)[N])
{
    puts(__PRETTY_FUNCTION__);
    return arr;
}

template <typename T, size_t N>
auto my_end(T (&arr)[N])
{
    puts(__PRETTY_FUNCTION__);
    return &arr[0] + N;
}

TEST_CASE("for-each for tables")
{
    int tab[10] = {1, 2, 3, 4, 5 };
    //vector<int> tab = { 1, 2, 3 };

    for(const auto& item : tab)
        cout << item << " ";
    cout << endl;

    for(auto it = my_begin(tab); it != my_end(tab); ++it)
    {
        const auto& item = *it;
        cout << item << " ";
    }

    cout << endl;
}
