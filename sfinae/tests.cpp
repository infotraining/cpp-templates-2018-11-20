#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace Explain
{
    template <bool Condition, typename KT_t = void>
    struct EnableIf
    {
        typedef KT_t type;
    };

    template <typename T>
    struct EnableIf<false, T>
    {};
}

//template <typename T>
//void calculate(T data)
//{
//    puts(__PRETTY_FUNCTION__);
//}

template <typename T>
typename Explain::EnableIf<!std::is_integral<T>::value>::type calculate(T data)
{
    puts(__PRETTY_FUNCTION__);
}

template <typename T>
typename Explain::EnableIf<std::is_integral<T>::value>::type calculate(T data)
{
    puts(__PRETTY_FUNCTION__);
}

void calculate(int data)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("sfinae")
{
    calculate(42);

    calculate(string("text"));

    calculate(3.14);

    short x = 42;
    calculate(x);
}








