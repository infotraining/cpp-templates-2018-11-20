#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <size_t N>
struct Factorial
{
    static const size_t value = Factorial<N-1>::value * N;
};

template <>
struct Factorial<0>
{
    static const size_t value = 1;
};

constexpr uintmax_t factorial(int n)
{
    return (n == 0) ? 1 : factorial(n-1) * n;
}


TEST_CASE("basic metaprogramming")
{
    static_assert(Factorial<14>::value == 87178291200);

    constexpr int n = 14;
    static_assert(factorial(n) == 87178291200);

}

/////////////////////////////////////////////////////
// loop unrolling
//

template <typename A, size_t N>
struct UnrollAssignment
{
    void operator()(A& a, const A& b) const
    {
        UnrollAssignment<A, N - 1>{}(a, b);

        cout << "assigment at index: " << N << " - " << a[N] << " = " << b[N] << '\n';

        a[N] = b[N];
    }
};

template <typename A>
struct UnrollAssignment<A, 0>
{
    void operator()(A& a, const A& b) const
    {
        cout << "assigment at index: " << 0 << " - " << a[0] << " = " << b[0] << '\n';

        a[0] = b[0];
    }
};

template <typename T, size_t N>
struct Array
{
    T items[N];

    template <typename Tab = Array>
    enable_if_t<(N >= 128), Tab&> operator=(const Tab& source)
    {
        if (this != &source)
        {
            for(size_t i = 0; i < size(); ++i)
                items[i] = source[i];

        }

        return *this;
    }

    template <typename Tab = Array>
    enable_if_t<(N < 128), Tab&> operator=(const Tab& source)
    {
        if (this != &source)
        {

//            items[0] = source[0];
//            items[1] = source[1];
//            items[2] = source[2];
//            ...

            UnrollAssignment<Array, N-1>{}(*this, source);
        }

        return *this;
    }

    constexpr size_t size() const
    {
        return N;
    }

    T& operator[](size_t index)
    {
        return items[index];
    }

    const T& operator[](size_t index) const
    {
        return items[index];
    }
};


TEST_CASE("loop unrolling")
{
    cout << "\n\n" << "Loop unrolling: \n";

    Array<int, 4> arr1 = { { 1, 2, 3, 4 } };
    Array<int, 4> arr2;

    arr2 = arr1;

    auto expected = {1, 2, 3, 4};
    REQUIRE(std::equal(begin(arr2.items), end(arr2.items), begin(expected)));
}


////////////////////////////////////////////
// Variadic templates
//

void k_print()
{
    cout << '\n';
}

template <typename Head, typename... Tail>
void k_print(Head head, Tail... tail)
{
    cout << (sizeof...(tail)) << ": ";

    cout << head << "; ";

    k_print(tail...);
}


TEST_CASE("variadic print")
{
    k_print(1, 3.14, "text", "abc"s);
    k_print("text", "abc"s);
}











