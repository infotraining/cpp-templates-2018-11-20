#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename T>
struct TypeSize
{
    static const size_t value = sizeof(T);
};

TEST_CASE("meta functions can return value")
{
    cout << TypeSize<int>::value << endl;
    static_assert(TypeSize<int>::value == 4, "Error");
}

template <typename C>
struct ElementT
{
    //using type = typename C::value_type;
    typedef typename C::value_type type;
};

template <typename T, size_t N>
struct ElementT<T[N]>
{
    typedef T type;
};

template <typename Container>
void process(const Container& c)
{
    using T = typename ElementT<Container>::type;

    // using T = decay_t<decltype(*begin(c))>; // C++14
}

TEST_CASE("metafunctions can return types")
{
    int vec[10];

    static_assert(is_same<ElementT<decltype(vec)>::type, int>::value, "types are different");
}

template <int N>
void is_ok()
{}

//template <double N>
//void ill_formed()
//{}

namespace Explain
{
    template <typename T, T v>
    struct IntegralConstant
    {
        static const T value = v;
        typedef T value_type;
    };

    template <bool v>
    using BoolConstant = IntegralConstant<bool, v>;

    using TrueType = BoolConstant<true>; // TrueType::value == true

    using FalseType = BoolConstant<false>; // TrueType::value == false


    ///////////////////////////////////
    /// IsVoid type trait
    ///

    template <typename T>
    struct IsVoid : FalseType
    {};

    template <>
    struct IsVoid<void> : TrueType
    {};

    //////////////////////////////////
    ///  IsPointer trait
    ///

    template <typename T>
    struct IsPointer : FalseType
    {};

    template <typename T>
    struct IsPointer<T*> : TrueType
    {};
}


TEST_CASE("integral constants")
{
    using namespace Explain;

    static_assert (IntegralConstant<int, 5>::value == 5, "Error");

    static_assert(is_same<IntegralConstant<int, 5>, IntegralConstant<int, 5>>::value, "Error");

    static_assert(IsVoid<void>::value, "Error");

    static_assert(IsPointer<const int*>::value, "Error");
}










