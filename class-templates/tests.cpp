#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct InPlace {};

constexpr InPlace in_place;

template <typename T>
class Holder
{
    T item;

public:
    explicit Holder(const T& obj)
        : item{obj} // copy
    {
        cout << "Holder(constructed using cc)" << endl;
    }

    explicit Holder(T&& obj)
        : item{std::move(obj)} // move
    {
        cout << "Holder(constructed using mc)" << endl;
    }

    template <typename... Arg>
    explicit Holder(InPlace, Arg&&... arg)
        : item(std::forward<Arg>(arg)...)
    {
        cout << "Holder(constructed in-place)" << endl;
    }

    T& get()
    {
        return item;
    }
};

template <typename T>
class Holder<T*>
{
    T* ptr_;
public:
    explicit Holder(T* ptr) : ptr_{ptr}
    {
        if (!ptr_)
            throw std::invalid_argument("ptr cannot be null");

        cout << "Holder(*: " << ptr_ << ")\n";
    }

    Holder(const Holder&) = delete;
    Holder& operator=(const Holder&) = delete;

    T& get()
    {
        return *ptr_;
    }

    ~Holder()
    {
        cout << "~Holder(*: " << ptr_ << ")\n";
        delete ptr_;
    }
};

template <>
class Holder<const char*>
{
    const char* str_;
public:
    explicit Holder(const char* str) : str_{str}
    {}

    const char* get()
    {
        return str_;
    }
};

TEST_CASE("Holder")
{
    string str = "Text1";
    Holder<string> hstr1(str);
    cout << hstr1.get() << endl;

    Holder<string> hstr2(std::move(str));
    cout << hstr2.get() << endl;

    Holder<string> hstr3(in_place, "Text in place");
    cout << hstr3.get() << endl;

    Holder<string> hstr4(in_place, 10u, '%');
    cout << hstr4.get() << endl;

    Holder<string*> ptr_str1(new string("on heap"));
    cout << ptr_str1.get() << endl;

    Holder<const char*> c_str("text");
    cout << c_str.get() << endl;

    Holder<unique_ptr<char[]>> dynamic_string(in_place, new char[100] { 'a', 'b' , 'c' });
}









