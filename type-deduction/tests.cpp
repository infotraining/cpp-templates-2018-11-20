#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>
#include <memory>

#include "catch.hpp"

using namespace std;

void foo(int) {}

template <typename T>
void deduce1(T arg)
{
    puts(__PRETTY_FUNCTION__);
}

template <typename T>
void deduce2(T& arg)
{
    puts(__PRETTY_FUNCTION__);
}

template <typename T>
void deduce3(T&& arg)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("Type deduction rules")
{
    int x = 10;
    int& ref_x = x;
    const int& cref_x = x;
    int tab[10];
    const int cx = 10;

    SECTION("Case 1")
    {
        deduce1(x);
        deduce1(ref_x); // ref is removed
        deduce1(cref_x); // ref & const are removed
        deduce1(tab); // int* - array decays to pointer
        deduce1(foo); // void (*)(int)
        deduce1(cx); // const is removed

        auto a1 = x;
        auto a2 = ref_x;
        auto a3 = cref_x;
        auto a4 = tab;
        auto a5 = foo;
        auto a6 = cx; // int
    }

    SECTION("Case 2")
    {
        cout << "\n";

        deduce2(x); // ref is added
        deduce2(ref_x); // ref is preserved
        deduce2(cref_x); // ref & const are preserved
        deduce2(tab); // tab size is preserved
        deduce2(foo); // void (&)(int)
        deduce2(cx); // const int&

        auto& a1 = x; // int&
        auto& a2 = ref_x; // int&
        auto& a3 = cref_x; // const int&
        auto& a4 = tab; // int(&a4)[10]
        auto& a5 = foo; // void (&)(int)
    }

    SECTION("Case 3")
    {
        cout << "\n";

        deduce3(x); // deduce3<int&>(int&)
        deduce3(ref_x); // deduce3<int&>(int&)
        deduce3(cref_x); // deduce<const int&>(const int&)
        deduce3(tab); // deduce3<int(&)[10]>(int(&)[10])
        deduce3(foo); // deduce3<void (&)(int)>(void (&)(int))
        deduce3(cx); // deduce3<const int&>(const int&)
        deduce3(string("text")); // deduce4<string>(string&&)

        auto&& a1 = x; // int&
        auto&& a2 = ref_x; // int&
        auto&& a3 = cref_x; // const int&
        auto&& a4 = tab; // int(&a4)[10]
        auto&& a5 = foo; // void (&)(int)
        auto&& a6 = cx;
        auto&& a7 = string("text");
    }
}

namespace Impl1
{
    template <typename Container, typename T>
    T my_accumulate(const Container& container, T init)
    {
        for (const auto& item : container)
            init += item;

        return init;
    }
}

namespace Impl2
{
    template <typename Container>
    auto my_accumulate(const Container& container)
    {
        using TResult = typename std::decay<decltype(*begin(container))>::type;

        TResult sum{};

        for (const auto& item : container)
            sum += item;

        return sum;
    }
}

TEST_CASE("accumulate")
{
    double tab[] = {1.1, 2.1, 3, 4, 5};

    REQUIRE(Impl1::my_accumulate(tab, 0.0) == Approx(15.2));
    REQUIRE(Impl2::my_accumulate(tab) == Approx(15.2));
}

struct Gadget
{
    string id;
};

void have_fun(Gadget& g)
{
    cout << "have_fun(G&: " << g.id << ")\n";
}

void have_fun(const Gadget& g)
{
    cout << "have_fun(const G&: " << g.id << ")\n";
}

void have_fun(Gadget&& g)
{
    cout << "have_fun(G&&: " << g.id << ")\n";
}

//void use(Gadget& g)
//{
//    have_fun(g);
//}

//void use(const Gadget& g)
//{
//    have_fun(g);
//}

//void use(Gadget&& g)
//{
//    have_fun(std::move(g));
//}

template <typename G>
void use(G&& g)
{
    //    if constexpr(is_reference<G>::value)
    //        have_fun(g);
    //    else
    //        have_fun(move(g));

    have_fun(std::forward<G>(g));
}

TEST_CASE("Perfect forwarding")
{
    cout << "\n\n";
    Gadget g{"g"};
    const Gadget cg{"cg"};

    have_fun(g);
    have_fun(cg);
    have_fun(Gadget{"temp g"});

    cout << "\n";

    use(g); // use<Gadget&>(Gadget&)
    use(cg); // use<const Gadget&>(const Gadget&)
    use(Gadget{"temp g"}); // use<Gadget>(Gadget&&)
}

namespace Legacy
{
    Gadget* create_gadget(string s)
    {
        return new Gadget{move(s)};
    }

    void use(Gadget* g)
    {
        cout << "using: " << g->id << endl;
    }

    void use_and_destoy(Gadget* g)
    {
        cout << "using: " << g->id << endl;

        delete g;
    }
}

namespace Simplified
{
    template <typename T>
    class K_unique_ptr_c
    {
        T* ptr_;

    public:
        explicit K_unique_ptr_c(T* ptr)
            : ptr_{ptr}
        {
        }

        K_unique_ptr_c(K_unique_ptr_c&& source)
            : ptr_{source.ptr_}
        {
            source.ptr_ = nullptr;
        }

        K_unique_ptr_c& operator=(K_unique_ptr_c&& source)
        {
            if (this != &source)
            {
                delete ptr_;

                ptr_ = source.ptr_;
                source.ptr_ = nullptr;
            }

            return *this;
        }

        T& operator*() const
        {
            return *ptr_;
        }

        template <typename U = T>
        typename std::enable_if<is_class<U>::value, U*>::type operator->() const
        {
            return ptr_;
        }

//        template <typename U = T>
//        std::enable_if_t<is_class_v<U>, U*> operator->() const
//        {
//            return ptr_;
//        }


        T* get() const
        {
            return ptr_;
        }

        T* release(T* new_ptr = nullptr)
        {
            T* result = ptr_;
            ptr_ = new_ptr;

            return result;
        }

        ~K_unique_ptr_c()
        {
            delete ptr_;
        }
    };

    K_unique_ptr_c<Gadget> create_gadget(string s)
    {
        return K_unique_ptr_c<Gadget>{new Gadget{move(s)}};
    }

    void use(Gadget* g)
    {
        cout << "using: " << g->id << endl;
    }

    void use(K_unique_ptr_c<Gadget> g)
    {
        cout << "using: " << g->id << endl;
    }

    template <typename T, typename... Arg>
    K_unique_ptr_c<T> make_k_unique_c(Arg&&... arg)
    {
        return K_unique_ptr_c<T>{ new T{std::forward<Arg>(arg)...}};
    }
}

TEST_CASE("unique_ptr")
{
    using namespace Simplified;

    K_unique_ptr_c<Gadget> ptr1 = make_k_unique_c<Gadget>("g on heap");

    cout << (*ptr1).id << endl;
    cout << ptr1->id << endl;

    use(ptr1.get());

    use(move(ptr1));
}
